
CC=gcc
CFLAGS=-Wall -g
LDFLAGS=-lGLU -lGL -lglut

OBJS=util.o game.o 

all: life

life: $(OBJS)
	$(CC) $(OBJS) -o life $(LDFLAGS)

game.o: game.c
	$(CC) $(CFLAGS) -c game.c

util.o: util.c
	$(CC) $(CFLAGS) -c util.c

clean:
	rm -rf *.o
