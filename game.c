#define _GNU_SOURCE

#include <GL/glut.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>

#include "util.h"

enum cell_state { ALIVE, NEXT_ALIVE, DEAD, NEXT_DEAD};
enum mouse_button { NONE, LEFT, RIGHT };

#define IS_ALIVE(c) (c == ALIVE || c == NEXT_DEAD)
#define IS_DEAD(c) (c == DEAD || c == NEXT_ALIVE)

struct board {

  int height;
  int width;

  int count;
  int generation;

  enum cell_state **cells;
};

struct bar {

  

};

#define ROW_START(a) 1
#define ROW_END(a) (a->height - 1)

#define COL_START(a) 1
#define COL_END(a) (a->width - 1)

#define CELL_PIXELS 8
#define CELL_DISTANCE_PIXELS 2

typedef struct board board;

struct rect {
  int w;
  int h;
};

typedef struct rect rect;

struct game {

  board *board;
  int period;
  int period_step;

  int running;
  int fullscreen;

  rect now;
  rect old;

  int cell_size;
  int cell_distance;
};

typedef struct game game;

#define BAR_HEIGHT 20
#define CELLS_TO_PIX(g, c) (c*(g->cell_size + g->cell_distance) + g->cell_distance)

static game *life;

board* board_create(int, int);
void   board_resize(board*, int, int);
void   board_iterate(board*);
void   board_destroy(board *);

void   game_reset(game*);
void   game_destroy(game*);

inline int game_is_fullscreen(game*);
inline int game_is_running(game*);
inline int game_run_toggle(game*);

inline int    game_get_period(game*);
inline void   game_dec_period(game *game);
inline void   game_inc_period(game *game);

void cb_timer(int num);
void cb_enter(int state);

void game_reset(game *game) {

  board *b = game->board;

  int i,j;
  for (i = ROW_START(b); i < ROW_END(b); i++)
    for (j = COL_START(b); j < COL_END(b); j++)
      b->cells[i][j] = DEAD;

  b->generation = 0;
  b->count = 0;
  return;
}

inline void game_inc_period(game *game) {

  if (game->period + game->period_step > 5000)
    return;

  game->period += game->period_step;
  return;
}

inline void game_dec_period(game *game) {

  if (game->period - game->period_step < 50)
    return;

  game->period -= game->period_step;
  return;
}

inline int game_get_period(game *game) {

  return game->period;
}

inline int game_is_fullscreen(game* game) {

  return game->fullscreen;
}

inline int game_round_height(game *game, int height) {
  
  int cs = game->cell_size;
  int cd = game->cell_distance;

  return ((height - cd) / (cd + cs)) * (cs + cd) + cd;
}

inline int game_round_width(game *game, int width) {

  int cs = game->cell_size;
  int cd = game->cell_distance;

  return ((width - cd) / (cd + cs)) * (cs + cd) + cd;
}

inline int game_is_running(game *game) {

  return game->running;
}

inline int game_run_toggle(game *game) {

  game->running = !game->running;
  return game->running;
}

inline void board_cell_set_state(board *board, int i, int j, enum cell_state state) {

  enum cell_state *cell = &board->cells[i][j];

  if (state == DEAD && *cell != DEAD) {
    *cell = DEAD;
    board->count--;
  } 
  
  if (state == ALIVE && *cell != ALIVE) {
    *cell = ALIVE;
    board->count++;
  }

  return;
}

void game_mouse_input(game *game, int x, int y, enum mouse_button button) {

  int col = x / (game->cell_size + game->cell_distance) + 1;
  int row = y / (game->cell_size + game->cell_distance) + 1;

  board *b = game->board;
  if (col < 1 || col >= (b->width - 1) ||
      row < 1 || row >= (b->height - 1)) 
      return;

  if (button == LEFT)
    board_cell_set_state(game->board, row, col, ALIVE);

  if (button == RIGHT)
    board_cell_set_state(game->board, row, col, DEAD);

  return;
}

game* game_create(void) {

  game *new = smalloc(sizeof(game));
  
  new->now.h = 0;
  new->now.h = 0;
  
  new->cell_size = CELL_PIXELS;
  new->cell_distance = CELL_DISTANCE_PIXELS;

  new->period = 300;
  new->period_step = 50;

  new->fullscreen = 0;
  new->running = 0;
  new->board = NULL;

  return new;
}

void game_destroy(game *game) {

        board_destroy(game->board);
        free(game);
        return;
}

void game_set_screen(game *game, int w, int h) {

        //  printf("set_screen_pixs: %d %d\n", w, h);

        int cols = (w - game->cell_distance) / (game->cell_size + game->cell_distance);
        int rows = (h - game->cell_distance) / (game->cell_size + game->cell_distance);

        if (game->board == NULL)
                game->board = board_create(cols, rows);
        else
                board_resize(game->board, cols, rows);

        //  printf("set_screen_cell: %d %d\n", rows, cols);

        game->now.w = w;
        game->now.h = h;
        return;
}

int game_fullscreen_toggle(game *game) {

        if (game_is_fullscreen(game)) {
                game->now = game->old;
        } else {
                game->old = game->now;
        }

        game->fullscreen = !game->fullscreen;

        return game->fullscreen;
}

void game_tick(game *game) {

        if (game->now.w == 0 || game->now.h == 0)
                return;

        board_iterate(game->board);
}

board* board_create(int width, int height) {

        int w = width + 2;
        int h = height + 2;

        board *new = smalloc(sizeof(board));
        new->cells = smalloc(sizeof(enum cell_state*)*h);

        int i;
        for (i = 0; i < h; i++) {
                new->cells[i] = smalloc(sizeof(enum cell_state)*w);
                int j;
                for (j = 0; j < w; ++j)
                        new->cells[i][j] = DEAD;
  }

  new->count = 0;
  new->height = h;
  new->width = w;
  new->generation = 0;

  return new;
}

void board_destroy(board *board) {

  int i;
  for (i = 0; i < board->height; i++)
    free(board->cells[i]);

  free(board->cells);
  free(board);

  return;
}

void board_resize(board *board, int width, int height) {

  int old_w = board->width;
  int old_h = board->height;

  int w = width + 2;
  int h = height + 2;

  board->cells = srealloc(board->cells, sizeof(enum cell_state*)*h);
  int i;
  for (i = old_h; i < h; i++)
    board->cells[i] = NULL;

  for (i = 0; i < h; i++) {
    int j = (board->cells[i] == NULL) ? 0 : old_w; 
    board->cells[i] = srealloc(board->cells[i], sizeof(enum cell_state)*w);
    for (; j < w; j++)
      board->cells[i][j] = DEAD;
  }

  board->width = w;
  board->height = h;

  return;
}

int board_cell_neighbours(board *board, int x, int y) {
  
  int bodies = 0;

  bodies += IS_ALIVE(board->cells[x-1][y-1]);
  bodies += IS_ALIVE(board->cells[x  ][y-1]);
  bodies += IS_ALIVE(board->cells[x+1][y-1]);

  bodies += IS_ALIVE(board->cells[x-1][y  ]);
  bodies += IS_ALIVE(board->cells[x+1][y  ]);

  bodies += IS_ALIVE(board->cells[x-1][y+1]);
  bodies += IS_ALIVE(board->cells[x  ][y+1]);
  bodies += IS_ALIVE(board->cells[x+1][y+1]);

  return bodies;
}

void board_iterate(board* board) {

  /*
   * at first mark cells which will be DEAD or ALIVE
   * in next generation
   */
  int i,j;
  for (i = ROW_START(board); i < ROW_END(board); i++) {
    for (j = COL_START(board); j < COL_END(board); j++) {

      int bodies = board_cell_neighbours(board, i, j);

      if (IS_ALIVE(board->cells[i][j]) && 
          (bodies < 2 || bodies > 3))
          board->cells[i][j] = NEXT_DEAD;
      
      if (IS_DEAD(board->cells[i][j]) &&
          (bodies == 3))
          board->cells[i][j] = NEXT_ALIVE;
    }
  }

  /*
   * complete transition to next generation
   */
  for (i = ROW_START(board); i < ROW_END(board); i++) {
    for (j = COL_START(board); j < COL_END(board); j++) {

      if (board->cells[i][j] == NEXT_ALIVE)
        board_cell_set_state(board, i, j, ALIVE);

      if (board->cells[i][j] == NEXT_DEAD)
        board_cell_set_state(board, i, j, DEAD);
    }
  }

  board->generation++;

  return;
}

void cb_resize(int width, int height) {

    int w = width;
    int h = height;

    printf("resize: %d %d\n", w, h);

    /*
     * set viewport and map space coordinates
     * to the window
     */
    glViewport(0, 0, w, h);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();           
    glOrtho(0, w, h, 0, -10, 10);

    game_set_screen(life, w, h - BAR_HEIGHT);
}

void cb_display(void) {

  /*
   * switch to the modelview so we can draw stuff 
   */
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  /*
   * clear the picture
   */
//  glClearColor(0.752941, 0.752941, 0.752941, 0.5);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  board *b = life->board;

  int i, j;
  for (i = ROW_START(b); i < ROW_END(b); i++) {
    for (j = COL_START(b); j < COL_END(b); j++) {

      double cr = 1;
      double cg = 0;
      double cb = 0;

      if (b->cells[i][j] == DEAD) {
        continue;
//        cr = cg = cb = 0.752941;
      }

      int size = life->cell_size;
      int x = (j-1)*(life->cell_size + life->cell_distance) + life->cell_distance;
      int y = (i-1)*(life->cell_size + life->cell_distance) + life->cell_distance;
 
      glPushMatrix();

      glTranslatef(x, y, 0);
      glBegin(GL_QUADS);
        glColor3f(cr, cg, cb);
        glVertex2f(0, 0);
        glVertex2f(0, size);
        glVertex2f(size, size);
        glVertex2f(size, 0);       
      glEnd();

      glTranslatef(1, 1, 0);
      glBegin(GL_QUADS);
        glColor3f(cr- 0.35, cg, cb);
        glVertex2f(0, 0);
        glVertex2f(0, size);
        glVertex2f(size, size);
        glVertex2f(size, 0);       
      glEnd();

      glPopMatrix();
    }
  }

  glColor3f(1, 1, 1);
  glRasterPos2i(5, life->now.h + BAR_HEIGHT - 5);


  char *str;

  char *fmt = "\
population: %d | \
period: %d | \
generation: %d";


  asprintf(&str, fmt,
           life->board->count, life->period, life->board->generation);

  char *c = str;
  while (*c) {
    glutBitmapCharacter(GLUT_BITMAP_9_BY_15, *c);
    c++;
  }

  free(str);

  glutSwapBuffers();
}

void cb_keyboard(unsigned char key, int x, int y) {

    if (key > 'A' && key <= 'Z')
      key = key + 'a' - 'A';

    switch (key) {
        case 'q':
            exit(0);
            break;
        case 'f':
            if (game_fullscreen_toggle(life)) 
              glutFullScreen();
            else
              glutReshapeWindow(life->now.w, life->now.h);
            break;
        case 'd':
            game_reset(life);
            glutPostRedisplay();
            break;
        case 'p':
            printf("count: %d\n", life->board->count);
            break;
        case 's':
            game_run_toggle(life);
            glutTimerFunc(game_get_period(life), cb_timer, 0);
            break;
        case 'i':
            if (!game_is_running(life)) {
              game_tick(life);
              glutPostRedisplay();
            }
            break;
         case '-':
            game_dec_period(life);
            glutPostRedisplay();
            break;
         case '+':
            game_inc_period(life);
            glutPostRedisplay();
            break;
       default:
            break;
    }
}

void cb_timer(int num) {

  if (!game_is_running(life))
    return;

  game_tick(life);
  glutPostRedisplay();
  
  printf("timer called: %d\n", game_get_period(life));

  glutTimerFunc(game_get_period(life), cb_timer, 0);

  return;
}

int button_l = 0;
int button_r = 0;

void cb_enter(int state) {

    if (state != GLUT_ENTERED)
      return;

    if (game_is_fullscreen(life))
      return;

    int width = glutGet(GLUT_WINDOW_WIDTH);
    int height = glutGet(GLUT_WINDOW_HEIGHT);

    int w = game_round_width(life, width);
    int h = game_round_height(life, height);

//    printf("width: %d == %d", w, width);
//    printf("width: %d == %d", h, height);

    if (w != width || h != height) {

      glutReshapeWindow(w, h);
      glutPostRedisplay();

      game_set_screen(life, w, h);
    }
}

void cb_mouse_motion(int x, int y) {

  enum mouse_button game_button = NONE;

  if (button_l) {
//    printf("[%d, %d]: button left\n", x, y);
    game_button = LEFT;
  }

  if (button_r) {
//    printf("[%d, %d]: button right\n", x, y);
    game_button = RIGHT;
  }

  if (game_button != NONE) { 
    game_mouse_input(life, x, y, game_button);
    glutPostRedisplay();
  }
}

void cb_mouse(int button, int state, int x, int y) {

  if (state == GLUT_DOWN) {
    if (button == GLUT_LEFT_BUTTON)
      button_l = 1;

    if (button == GLUT_RIGHT_BUTTON)
      button_r = 1;
  }

  if (state == GLUT_UP) {
    if (button == GLUT_LEFT_BUTTON)
      button_l = 0;

    if (button == GLUT_RIGHT_BUTTON)
      button_r = 0;
  }
 

  enum mouse_button game_button = NONE;

  if (button == GLUT_LEFT_BUTTON) {
//    printf("[%d, %d]: button left\n", x, y);
    game_button = LEFT;
  }

  if (button == GLUT_RIGHT_BUTTON) {
//    printf("[%d, %d]: button right\n", x, y);
    game_button = RIGHT;
  }

  if (game_button != NONE) {
    game_mouse_input(life, x, y, game_button);
    glutPostRedisplay();
  }
}

#define DEFAULT_HEIGHT 60
#define DEFAULT_WIDTH 100

int main (int argc, char **argv) {

    life = game_create();

    glutInit(&argc,argv);

    glutInitDisplayMode(GLUT_RGBA | GLUT_ALPHA | GLUT_DOUBLE);

    glutInitWindowSize(CELLS_TO_PIX(life, DEFAULT_WIDTH), 
                       CELLS_TO_PIX(life, DEFAULT_HEIGHT));

    printf("%d\n", CELLS_TO_PIX(life, DEFAULT_HEIGHT));

    glutInitWindowPosition(10, 10);

    glutCreateWindow("life");

    glutDisplayFunc(cb_display);
    glutReshapeFunc(cb_resize);
    glutKeyboardFunc(cb_keyboard);
    glutMouseFunc(cb_mouse);
    glutMotionFunc(cb_mouse_motion);
    glutEntryFunc(cb_enter);

    //glutTimerFunc(500, cb_timer, 0);

    glutMainLoop();

    return 0;
}

